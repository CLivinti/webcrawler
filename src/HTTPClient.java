import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;

public class HTTPClient {
	private final String USER_AGENT = "RIWEB_CRAWLER";
	private StringBuilder messRequest;
	private String urlRequest;
	private StringBuilder resourceContent;
	
	public HTTPClient(String url)
	{
		this.urlRequest = url;
		this.resourceContent = new StringBuilder();
	}
	
	public SimpleEntry<String, String> extractHostAndPageFromUrl()
	{
		String domain = "";
		String page = "/";
		
		int indexStart = 0;
		int indexStop = 0;
		for(int i = 0; i < urlRequest.length(); ++i)
		{
			if(urlRequest.charAt(i) == '/' && urlRequest.charAt(i+1) != '/')
			{
				indexStart = i + 1;
				break;
			}
		}
		
		for(int i = indexStart; i < urlRequest.length(); ++i)
		{
			if(urlRequest.charAt(i) != '/')
			{
				domain += urlRequest.charAt(i);
			}
			else
			{
				indexStop = i;
				break;
			}
		}
		
		if(indexStop != 0)
		{
			for(int i = indexStop + 1; i < urlRequest.length(); ++i)
			{
				page += urlRequest.charAt(i);
			}
		}
		
		return new SimpleEntry<String, String>(domain, page);
	}
	
	private void buildRequest()
	{		
		SimpleEntry<String,String> domainAndPage = extractHostAndPageFromUrl();
		
		String host = domainAndPage.getKey();
		String page = domainAndPage.getValue();
		
		messRequest = new StringBuilder();
		messRequest = messRequest.append("GET " + page + " HTTP/1.1\r\n");
		messRequest = messRequest.append("Host:" + host + "\r\n");
		messRequest = messRequest.append("User-Agent:" + USER_AGENT + "\r\n");
		messRequest = messRequest.append("\r\n");
	}
	
	public void sendRequest() throws Exception
	{
		buildRequest();
		URL url = new URL(this.urlRequest);
		Socket socket = new Socket(url.getHost(), 80);
		
		PrintWriter pw = new PrintWriter(socket.getOutputStream());
		//System.out.println(messRequest);
		pw.println(messRequest);
		pw.flush();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		String line = br.readLine();
		
		if(line != null)
		{
			if(line.startsWith("HTTP/1.1"))
			{
				System.out.println(line);
				ArrayList<String> components = splitTextBySpaces(line);
				String codStare = components.get(1);
				if(codStare.charAt(0) == '4' || codStare.charAt(0) == '5')
				{
					BufferedWriter bw = new BufferedWriter(new FileWriter("eroare.txt"));
					bw.write(messRequest.toString());
					bw.write("\n");
					bw.write(line + "\n");
					line = br.readLine();
					while(line != null)
					{
						bw.write(line + "\n");
						line = br.readLine();
					}
					
					bw.close();
				} 
				else if(codStare.charAt(0) == '3')
				{
					line = br.readLine();
					while(!line.isEmpty())
					{
						if(line.startsWith("Location: "))
						{
							String newLocation = line.substring("Location: ".length());
							System.out.println("New Location: " + newLocation);
							this.urlRequest = newLocation;
							sendRequest();
						}
						line = br.readLine();
					}
				}
				else if(codStare.equals("200"))
				{
					line = br.readLine();
					while(!line.isEmpty()) { line = br.readLine(); }
					
					line = br.readLine();
					while(line != null)
					{
						//System.out.println(line);
						resourceContent.append(line + "\n");
						line = br.readLine();
					}
					
				}
			}
		}
		
		br.close();
		pw.close();
		socket.close();
	}
	
	public StringBuilder getPageContent()
	{
		try {
			sendRequest();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resourceContent;
	}
	
	private ArrayList<String> splitTextBySpaces(String text)
	{
		ArrayList<String> words = new ArrayList<String>();
		
		String word = "";
		
		for(int i = 0; i < text.length(); ++i)
		{
			if(i == text.length() - 1)
			{
				if(text.charAt(i) != ' ')
				{
					word += text.charAt(i);
					words.add(word);
				}
			}
			
			if(text.charAt(i) != ' ')
				word += text.charAt(i);
			else
			{
				words.add(word);
				word = "";
			}
		}
		
		return words;
	}
}
