import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class SequantialWebCrawler {
	
	private Queue<String> unvisitedLinks = new LinkedList<String>();
	private HashMap<String,Boolean> visitedLinks = new HashMap<String, Boolean>();
	private HashMap<String, Boolean> firstExploration = new HashMap<String, Boolean>();
	private int maxPages;
	
	public SequantialWebCrawler(String startUrl, int max)
	{
		unvisitedLinks.add(startUrl);
		maxPages = max;
	}
	
	public void startCrawl() throws MalformedURLException, URISyntaxException
	{
		int nrPag = 0;
		while(!unvisitedLinks.isEmpty() && nrPag < maxPages)
		{
			String url = unvisitedLinks.poll();
			System.out.println("URL:" + url);
			if(visitedLinks.containsKey(url) && visitedLinks.get(url) == true)
			{
				continue;
			}
			else
			{
				nrPag++;
				
				visitedLinks.put(url, true);
				
				String domain = getDomainFromURL(url);
				
				//Verific daca domeniul este la prima explorare si daca este atunci extrag robots.txt
				if(!firstExploration.containsKey(domain))
				{
					DNSClient dns = new DNSClient(domain);
					dns.sendRequest();
					//String address = dns.getIpAddress();
					
					HTTPClient http = new HTTPClient("http://" + domain + "/robots.txt");
					StringBuilder robotsContent = http.getPageContent();
					Boolean canExplore = checkIfCanExplore(robotsContent);
					
					firstExploration.put(domain, canExplore);
					
					//Daca nu poate fi explorat atunci trec la urmatorul link din coada
					if(!canExplore)
						continue;
					
					System.out.println("Am voie sa explorez domeniul");
				}
				else
				{
					//Daca domeniul nu e la prima explorare dar totusi nu am voie sa-l analizez atunci trec la urmatorul link din coada.
					if(!firstExploration.get(domain))
						continue;
				}
				
				String page = getPageFromURL(url);
				System.out.println("Page: " + page);
				
				DNSClient dns = new DNSClient(domain);
				dns.sendRequest();
				//String address = dns.getIpAddress();
				
				HTTPClient http = new HTTPClient("http://" + domain + page);
				StringBuilder pageContent = http.getPageContent();
				SimpleEntry<String,String> hostAndPage = http.extractHostAndPageFromUrl();
				//String host = hostAndPage.getKey();
				page = hostAndPage.getValue();
				
				if(pageContent.length() > 0)
				{	
					Document doc = Jsoup.parse(pageContent.toString());
					
					Elements metaTags = doc.select("meta");
					String contentMetaRobots = null;
					
					for(int i = 0; i < metaTags.size(); ++i)
					{
						String nameRobots = metaTags.get(i).attr("name");
						if(nameRobots.equals("robots"))
						{
							contentMetaRobots = metaTags.get(i).attr("content");
							break;
						}
					}
					
					if(contentMetaRobots == null || contentMetaRobots.equals("follow"))
					{
						analyzeALinksFromDocument(doc, url);
					}
					
					if(page == "/")
					{
						if(url.endsWith("/"))
							url = url.substring(0, url.length() - 1);
						new File(url).mkdirs();
						File file = new File(url + "/" + url + ".html");
						try {
							BufferedWriter writer = new BufferedWriter(new FileWriter(file));
							writer.write(pageContent.toString());
							writer.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else
					{
						writePageContentInFile(url, pageContent);
					}
					
					System.out.println("\n");
					
				}
			}
		}
	}
	
	private Boolean checkIfCanExplore(StringBuilder robotsContent)
	{	
		String[] lines = robotsContent.toString().split("\\n");
		
		for(int i = 0; i < lines.length; ++i)
		{
			if(!lines[i].isEmpty())
			{
				String[] components = lines[i].split(":");
				if(components[0].equals("User-agent") && (components[1].equals(" *") || components[1].equals(" RIWEB_CRAWLER")))
				{
					components = lines[i + 1].split(":");
					if(components.length == 1 && components[0].equals("Disallow"))
					{
						return true;
					}
					else if(components.length == 2 && components[0].equals("Allow") && components[1].equals(" /"))
					{
						return true;
					}
				
				}
			}
		}
		
		return false;
	}
	
	private void analyzeALinksFromDocument(Document doc, String url) throws URISyntaxException, MalformedURLException
	{
		Elements links = doc.select("a");
		for(int i = 0; i < links.size(); ++i)
		{
			String relAttribLink = links.get(i).attr("rel");
			if(relAttribLink.isEmpty() || !relAttribLink.equals("nofollow"))
			{
				String link = links.get(i).attr("href");
				
				if(!link.isEmpty())
				{	
					if(!link.contains(" "))
					{
						URI ur = new URI(link);
						if(!ur.isAbsolute())
						{
							URL baseUrl = new URL("http://" + url);
							URL u = new URL(baseUrl, link);
							link = u.toString();
						}
					}
					
					int index = link.indexOf("#");
					if(index != -1)
						link = link.substring(0,index);
					if(link.endsWith("/"))
						link = link.substring(0, link.length() - 1);
					if(link.startsWith("http://"))
						link = link.substring("http://".length());
					
					if(!visitedLinks.containsKey(link))
					{
						visitedLinks.put(link, false);
						unvisitedLinks.add(link);
					}
				}
			}
		}
	}
	
	private String getDomainFromURL(String url)
	{
		String domain = "";
		
		int i = 0;
		while(i < url.length() && url.charAt(i) != '/')
		{
			domain += url.charAt(i);
			i++;
		}
		
		return domain;
	}
	
	private String getPageFromURL(String url)
	{
		String page = "";
		
		int i = 0;
		while(i < url.length() && url.charAt(i) != '/')
			i++;
		
		page = url.substring(i);
		return page;
	}
	
	private void writePageContentInFile(String url, StringBuilder pageContent)
	{
		if(url.endsWith("/"))
		{
			url = url.substring(0, url.length() - 1);
		}
		
		int index = url.lastIndexOf('/');
		String lastPage = url.substring(index);
		
		if(!lastPage.endsWith(".html"))
			lastPage += ".html";
		
		new File(url).mkdirs();
		File file = new File(url + lastPage);
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			bw.write(pageContent.toString());
			bw.close();
		}  catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
