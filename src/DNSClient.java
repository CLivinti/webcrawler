import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class DNSClient {

	private String webResource;
	private byte[] bufferRequest;
	private int bufferSize;
	private String ipAddress;
	
	public DNSClient(String resource)
	{
		webResource = resource;
		allocateBufferRequest();
		ipAddress = new String();
	}
	
	private void allocateBufferRequest()
	{
		bufferSize = 12;	//12 octeti pentru header-ul cererii DNS
		bufferSize += 4;	//+ 4 octeti pentru QType si QClass	
		int lengthDomainName = this.webResource.length() + 2;
		
		bufferSize += lengthDomainName; //+ lungimea numelui de domeniu
		
		this.bufferRequest = new byte[bufferSize];
	}
	
	private void setHeader()
	{
		this.bufferRequest[0] = 0;
		this.bufferRequest[1] = 7;
		
		byte workByte = 0;
		
		int pos = 7;
		
		workByte &= ~(1 << pos);
		
		pos = 6;
		
		while(pos >= 3)
		{
			workByte &= ~(1 << pos);
			pos--;
		}
		
		workByte &= ~(1 << pos--);
		workByte &= ~(1 << pos--);
		workByte &= ~(1 << pos--);
		
		this.bufferRequest[2] = workByte;
		
		workByte = 0;
		this.bufferRequest[3] = workByte;
		
		workByte = 0;
		this.bufferRequest[4] = workByte;
		workByte = 1;
		this.bufferRequest[5] = workByte;
		
		workByte = 0;
		pos = 6;
		while(pos <= 12)
		{
			this.bufferRequest[pos++] = 0;
		}
	}
	
	private void setQuestion()
	{
		//Set Question Name
		int pos = 12;
		String[] components = webResource.split("\\.");
		
		for(int i = 0; i < components.length; ++i)
		{
			int length = components[i].length();
			this.bufferRequest[pos++] = (byte) length;
			for(int j = 0; j < length; ++j)
			{
				this.bufferRequest[pos++] = (byte)components[i].charAt(j);
			}
		}
		
		this.bufferRequest[pos++] = 0;
		
		//Set QType
		this.bufferRequest[pos++] = 0;
		this.bufferRequest[pos++] = 1;
		
		//Set QClass
		this.bufferRequest[pos++] = 0;
		this.bufferRequest[pos] = 1;
		
		/*System.out.println("Sending: " + bufferRequest.length + " bytes");
        for (int i =0; i< bufferRequest.length; i++) {
            System.out.print("0x" + String.format("%x", bufferRequest[i]) + " " );
        }*/
	}
	
	public void sendRequest()
	{
		setHeader();
		setQuestion();
		
		int port = 53;
		String ipHost = "81.180.223.1";
		
		int nrBiti = 4;
		
		try {
			DatagramSocket clientSocket = new DatagramSocket();
			
			InetAddress IPAddress = InetAddress.getByName(ipHost);
			
			DatagramPacket sendPacket = new DatagramPacket(this.bufferRequest, this.bufferRequest.length, IPAddress, port);
			clientSocket.send(sendPacket);
			
			byte[] receiveData = new byte[512];
			DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
			clientSocket.receive(receivePacket);
			
			DataInputStream din = new DataInputStream(new ByteArrayInputStream(receiveData));
			din.readShort(); //read ID
	        
	        short flags = din.readShort(); //read flags 
	        short RCode = (short) (flags & (1 << nrBiti));
	        if(RCode != 0)
	        {
	        	System.out.println("Eroare la primirea mesajului: RCode = " + RCode);
	        }
	        
	        //System.out.println("Flags: 0x" + String.format("%x", flags));
	        din.readShort();
	        //System.out.println("Questions: 0x" + String.format("%x", questions));
	        
	        short ANCount = din.readShort();
	        if((ANCount & 1) < 1)
	        {
	        	System.out.println("Nu s-a primit nici un raspuns de la server!");
	        }
	        
	        //System.out.println("Answers RRs: 0x" + String.format("%x", ANCount));
	        din.readShort();
	        //System.out.println("Authority RRs: 0x" + String.format("%x", authorityRRs));
	        din.readShort();
	        //System.out.println("Additional RRs: 0x" + String.format("%x", additionalRRs));
			
	        String name = "";
	        int recLen = 0;
	        while ((recLen = din.readByte()) > 0) {
	            byte[] record = new byte[recLen];

	            for (int i = 0; i < recLen; i++) {
	                record[i] = din.readByte();
	            }
	            
	            name += new String(record, "UTF-8");
	            name += ".";
	            //System.out.println("Record: " + new String(record, "UTF-8"));
	        }
	        
	        name = name.substring(0, name.length() - 1);
	        //System.out.println("Name:" + name);
	        
	        short type = din.readShort();
	        if(type != 1)
	        {
	        	System.out.println("Raspunsul primit nu contine o adresa IPv4");
	        }
	        //System.out.println("Record Type: 0x" + String.format("%x", type));
	        din.readShort();
	        //System.out.println("Class: 0x" + String.format("%x", clasa));

	        din.readShort();
	        //System.out.println("Field: 0x" + String.format("%x", field));
	        din.readShort();
	        //System.out.println("Type: 0x" + String.format("%x", type));
	        din.readShort();
	        //System.out.println("Class: 0x" + String.format("%x", clasa));
	        din.readInt(); //read TTL
	        //System.out.println("TTL: 0x" + String.format("%x", TTL));

	        short addrLen = din.readShort();
	        //System.out.println("Len: 0x" + String.format("%x", addrLen));

	        if(addrLen != 4)
	        {
	        	System.out.println("Raspunsul primit nu contine o adresa IPv4");
	        }
	        
	        for (int i = 0; i < addrLen; i++ ) {
	        	short temp = (short) (din.readByte() & 0xFF);
	            ipAddress += temp + ".";
	        }
	        
	        ipAddress = ipAddress.substring(0, ipAddress.length() - 1);
			
			clientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getIpAddress()
	{
		return ipAddress;
	}
}